package ru.akv.oapi2jschema.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.akv.oapi2jschema.core.exception.InvalidFormatException;
import ru.akv.oapi2jschema.core.exception.OApi2SchemaException;
import ru.akv.oapi2jschema.core.exception.UnknownElementException;
import ru.akv.oapi2jschema.core.exception.UnsupportedOApiVersionException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

@SuppressWarnings("unused")
public class Converter {
    public enum SupportedOpenApiVersions {
        v3_0("3.0");

        private final String prefix;

        SupportedOpenApiVersions(String prefix) {
            this.prefix = prefix;
        }

        @Override
        public String toString() {
            return prefix;
        }
    }

    private static final ObjectMapper objectMapper = new YAMLMapper();
    private final Logger log = LoggerFactory.getLogger(Converter.class);

    public void process(InputStream in, OutputStream out) throws IOException {
        process(in, out, (name) -> true);
    }

    public void process(InputStream in, OutputStream out, List<String> namesToAdd) throws IOException {
        process(in, out, namesToAdd::contains);
    }

    private void process(InputStream in, OutputStream out, Predicate<String> predToAdd) throws IOException {
        var rootNode = objectMapper.readTree(in);
        var oapiVer = checkVersion(rootNode);
        log.info("Версия OpenAPI: {}", oapiVer);
        var componentsNode = rootNode.path("components");
        var schemaNode = objectMapper.createObjectNode();
        schemaNode.put("$schema", "https://json-schema.org/draft/2019-09/schema");
        var defsNode = schemaNode.putObject("$defs");
        var oneOfNode = schemaNode.putArray("oneOf");
        processSchemas(componentsNode, (name, node) -> {
            defsNode.set(name, node);
            if (predToAdd.test(name)) {
                oneOfNode.addObject().put("$ref", "#/$defs/" + name);
                log.info("Добавлена схема \"{}\"", name);
            }
        });
        out.write(schemaNode.toString().getBytes(StandardCharsets.UTF_8));
    }

    private SupportedOpenApiVersions checkVersion(JsonNode rootNode) {
        var versionNode = rootNode.get("openapi");
        if (versionNode == null)
            throw new InvalidFormatException("Не найден элемент \"openapi\"");
        var ver = versionNode.asText();
        for (var elem : SupportedOpenApiVersions.values()) {
            if (ver.startsWith(elem.toString()))
                return elem;
        }
        throw new UnsupportedOApiVersionException(ver);
    }

    private void processSchemas(JsonNode componentsNode, BiConsumer<String, ? super JsonNode> action) {
        var nodeSchemas = componentsNode.get("schemas");
        if (nodeSchemas == null) {
            log.warn("Узел \"schemas\" отсутствует");
            return;
        }
        if (nodeSchemas.isEmpty()) {
            log.info("Узел \"schemas\" пустой");
            return;
        }
        nodeSchemas.fields().forEachRemaining((field) -> {
            var name = field.getKey();
            var nodeField = field.getValue();
            processField(name, nodeField, action);
        });
    }

    private void processField(String name, JsonNode fieldNode, BiConsumer<String, ? super JsonNode> action) {
        var nodeRes = objectMapper.createObjectNode();
        try {
            if (fieldNode.has("type"))
                fillType(fieldNode, nodeRes);
            else if (fieldNode.has("$ref"))
                fillReference(fieldNode, nodeRes);
            else if (fieldNode.has("oneOf")
                    || fieldNode.has("anyOf")
                    || fieldNode.has("allOf"))
                fillComposition(fieldNode, nodeRes);
            action.accept(name, nodeRes);
        } catch (OApi2SchemaException ex) {
            log.warn("Поле \"{}\": {}", name, ex.getMessage());
        }
    }

    private void fillType(JsonNode nodeSrc, ObjectNode nodeRes) {
        fillCommonsForSchemaObject(nodeSrc, nodeRes);
        var type = nodeSrc.path("type").textValue();
        switch (type) {
            case "boolean" -> fillForBoolean(nodeSrc, nodeRes);
            case "integer" -> fillForInteger(nodeSrc, nodeRes);
            case "number" -> fillForNumber(nodeSrc, nodeRes);
            case "string" -> fillForString(nodeSrc, nodeRes);
            case "array" -> fillForArray(nodeSrc, nodeRes);
            case "object" -> fillForObject(nodeSrc, nodeRes);
            default -> throw new UnknownElementException("Тип \"" + type + "\" не поддерживается");
        }
    }

    private void fillReference(JsonNode nodeSrc, ObjectNode nodeRes) {
        var ref = nodeSrc.get("$ref").textValue();
        var slashPos = ref.lastIndexOf('/');
        if (slashPos != -1)
            ref = ref.substring(slashPos + 1);
        if (ref.isEmpty())
            throw new UnknownElementException("Ссылка пустая");
        nodeRes.put("$ref", "#/$defs/" + ref);
    }

    private void fillComposition(JsonNode nodeSrc, ObjectNode nodeRes) {
        fillCommonsForSchemaObject(nodeSrc, nodeRes);
        ArrayNode nodeArr;
        JsonNode nodeValues = null;
        if (nodeSrc.has("oneOf")) {
            nodeArr = nodeRes.putArray("oneOf");
            nodeValues = nodeSrc.get("oneOf");
        } else if (nodeSrc.has("anyOf")) {
            nodeArr = nodeRes.putArray("anyOf");
            nodeValues = nodeSrc.get("anyOf");
        } else if (nodeSrc.has("allOf")) {
            nodeArr = nodeRes.putArray("allOf");
            nodeValues = nodeSrc.get("allOf");
        } else {
            nodeArr = null;
        }
        if ((nodeValues == null) || (!nodeValues.isArray()))
            throw new InvalidFormatException("Для composition-элемента не задано ни одного значения");
        nodeValues.elements().forEachRemaining((elem) ->
            processField(null, elem, (name, value) -> nodeArr.add(value))
        );
    }

    private void fillCommonsForSchemaObject(JsonNode nodeSrc, ObjectNode nodeRes) {
        var field = nodeSrc.path("title").textValue();
        if (field != null)
            nodeRes.put("title", field);
        field = nodeSrc.path("description").textValue();
        if (field != null)
            nodeRes.put("description", field);
    }

    private void fillTypeValue(String type, boolean isNullable, ObjectNode nodeRes) {
        if (isNullable) {
            var typesNode = nodeRes.putArray("type");
            typesNode.add(type);
            typesNode.add("null");
        } else
            nodeRes.put("type", type);
    }

    private void fillForBoolean(JsonNode nodeSrc, ObjectNode nodeRes) {
        fillTypeValue("boolean", nodeSrc.path("nullable").booleanValue(), nodeRes);
    }

    private void fillForInteger(JsonNode nodeSrc, ObjectNode nodeRes) {
        fillTypeValue("integer", nodeSrc.path("nullable").booleanValue(), nodeRes);
        boolean hasMin = false, hasMax = false;
        if (nodeSrc.has("maximum")) {
            hasMax = true;
            // For OAS version 3.0 'exclusiveMaximum' is boolean
            nodeRes.put(nodeSrc.path("exclusiveMaximum").booleanValue() ? "exclusiveMaximum" : "maximum",
                    nodeSrc.get("maximum").longValue());
        }
        if (nodeSrc.has("minimum")) {
            hasMin = true;
            // For OAS version 3.0 'exclusiveMinimum' is boolean
            nodeRes.put(nodeSrc.path("exclusiveMinimum").booleanValue() ? "exclusiveMinimum" : "minimum",
                    nodeSrc.get("minimum").longValue());
        }
        if (nodeSrc.has("multipleOf")) {
            nodeRes.put("multipleOf", nodeSrc.get("multipleOf").longValue());
        }
        if (nodeSrc.has("format")) {
            var format = nodeSrc.get("format").textValue();
            if (format.equals("int32")) {
                if (!hasMin)
                    nodeRes.put("minimum", Integer.MIN_VALUE);
                if (!hasMax)
                    nodeRes.put("maximum", Integer.MAX_VALUE);
            } else if (format.equals("int64")) {
                if (!hasMin)
                    nodeRes.put("minimum", Long.MIN_VALUE);
                if (!hasMax)
                    nodeRes.put("maximum", Long.MAX_VALUE);
            }
        }
    }

    private void fillForNumber(JsonNode nodeSrc, ObjectNode nodeRes) {
        fillTypeValue("number", nodeSrc.path("nullable").booleanValue(), nodeRes);
        if (nodeSrc.has("maximum")) {
            nodeRes.put("maximum", nodeSrc.get("maximum").longValue());
        }
        if (nodeSrc.has("exclusiveMaximum")) {
            nodeRes.put("exclusiveMaximum", nodeSrc.get("exclusiveMaximum").longValue());
        }
        if (nodeSrc.has("minimum")) {
            nodeRes.put("minimum", nodeSrc.get("minimum").longValue());
        }
        if (nodeSrc.has("exclusiveMinimum")) {
            nodeRes.put("exclusiveMinimum", nodeSrc.get("exclusiveMinimum").longValue());
        }
        if (nodeSrc.has("multipleOf")) {
            nodeRes.put("multipleOf", nodeSrc.get("multipleOf").longValue());
        }
    }

    private void fillForString(JsonNode nodeSrc, ObjectNode nodeRes) {
        fillTypeValue("string", nodeSrc.path("nullable").booleanValue(), nodeRes);
        if (nodeSrc.has("maxLength")) {
            nodeRes.put("maxLength", nodeSrc.get("maxLength").longValue());
        }
        if (nodeSrc.has("minLength")) {
            nodeRes.put("minLength", nodeSrc.get("minLength").longValue());
        }
        boolean hasPattern = false;
        if (nodeSrc.has("pattern")) {
            hasPattern = true;
            nodeRes.put("pattern", nodeSrc.get("pattern").textValue());
        }
        if (nodeSrc.has("format")) {
            var format = nodeSrc.get("format").textValue();
            switch (format) {
                case "byte" -> {
                    if (!hasPattern)
                        nodeRes.put("pattern", "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$");
                }
                case "binary" -> {
                    if (!hasPattern)
                        nodeRes.put("pattern", "^[0-9 A-F a-f]*$");
                }
                default -> nodeRes.put("format", format);
            }
        }
    }

    private void fillForArray(JsonNode nodeSrc, ObjectNode nodeRes) {
        fillTypeValue("array", nodeSrc.path("nullable").booleanValue(), nodeRes);
        JsonNode nodeItems = nodeSrc.get("items");
        if (nodeItems == null)
            throw new InvalidFormatException("Отсутствует поле \"items\" для массива");
        processField("items", nodeItems, nodeRes::set);
        var nodeTmp = nodeSrc.get("maxItems");
        if (nodeTmp != null)
            nodeRes.set("maxItems", nodeTmp);
        nodeTmp = nodeSrc.get("minItems");
        if (nodeTmp != null)
            nodeRes.set("minItems", nodeTmp);
        nodeTmp = nodeSrc.get("uniqueItems");
        if (nodeTmp != null)
            nodeRes.set("uniqueItems", nodeTmp);
    }

    private void fillForObject(JsonNode nodeSrc, ObjectNode nodeRes) {
        fillTypeValue("object", nodeSrc.path("nullable").booleanValue(), nodeRes);
        var nodeProperties = nodeSrc.get("properties");
        if (nodeProperties != null) {
            var nodeJProperties = nodeRes.putObject("properties");
            nodeProperties.fields().forEachRemaining((field) -> {
                var name = field.getKey();
                var nodeField = field.getValue();
                processField(name, nodeField, nodeJProperties::set);
            });
        }
        var nodeTmp = nodeSrc.get("additionalProperties");
        if (nodeTmp != null)
            nodeRes.set("additionalProperties", nodeTmp);
        nodeTmp = nodeSrc.get("required");
        if (nodeTmp != null)
            nodeRes.set("required", nodeTmp);
        nodeTmp = nodeSrc.get("maxProperties");
        if (nodeTmp != null)
            nodeRes.set("maxProperties", nodeTmp);
        nodeTmp = nodeSrc.get("minProperties");
        if (nodeTmp != null)
            nodeRes.set("minProperties", nodeTmp);
    }
}
