package ru.akv.oapi2jschema.core.exception;

public class InvalidFormatException extends OApi2SchemaException {
    public InvalidFormatException(String msg) {
        super(msg);
    }

    public InvalidFormatException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
