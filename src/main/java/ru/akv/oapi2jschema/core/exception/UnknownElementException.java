package ru.akv.oapi2jschema.core.exception;

public class UnknownElementException extends OApi2SchemaException {
    public UnknownElementException(String message) {
        super(message);
    }
}
