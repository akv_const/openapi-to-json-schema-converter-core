package ru.akv.oapi2jschema.core.exception;

public abstract class OApi2SchemaException extends RuntimeException {
    protected OApi2SchemaException(String msg) {
        super(msg);
    }

    protected OApi2SchemaException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
