package ru.akv.oapi2jschema.core.exception;

public class UnsupportedOApiVersionException extends OApi2SchemaException {
    public UnsupportedOApiVersionException(String version) {
        super("Версия openapi " + version + " не поддерживается");
    }
}
