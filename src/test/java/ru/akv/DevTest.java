package ru.akv;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ru.akv.oapi2jschema.core.Converter;

import java.io.ByteArrayOutputStream;
import java.util.List;

@Disabled
public class DevTest {
    @Test
    public void ReadJsonTest() {
        try (var inputStream = this.getClass().getResourceAsStream("/openapi.yaml");
             var outputStream = new ByteArrayOutputStream()) {
            var conv = new Converter();
            conv.process(inputStream, outputStream, List.of("OrganizationInfo", "FirstPerson", "FindItem", "PdfResponse"));
            System.out.println(outputStream);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
